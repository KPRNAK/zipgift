//
//  GiftCard.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import Foundation
import RealmSwift

class GiftCard: Object, Codable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var vendor: String = ""
    @objc dynamic var brand: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var discount: Double = 0.0
    @objc dynamic var terms: String = ""
    @objc dynamic var position: Int = 0
    @objc dynamic var addedInCart: Bool = false
    var denominations: List<Denomination> = List<Denomination>()
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func decode(_ data: Data) -> [GiftCard] {
        do {
            var result: [GiftCard] = []
            if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: Any]] {
                json.forEach { eachJson in
                    let newCard = GiftCard()
                    if let id = eachJson["id"] as? String {
                        newCard.id = id
                    }
                    if let vendor = eachJson["vendor"] as? String {
                        newCard.vendor = vendor
                    }
                    if let brand = eachJson["brand"] as? String {
                        newCard.brand = brand
                    }
                    if let image = eachJson["image"] as? String {
                        newCard.image = image
                    }
                    if let discount = eachJson["discount"] as? Double {
                        newCard.discount = discount
                    }
                    if let terms = eachJson["terms"] as? String {
                        newCard.terms = terms
                    }
                    if let position = eachJson["position"] as? Int {
                        newCard.position = position
                    }
                    if let deno = eachJson["denominations"] as? [[String: Any]] {
                        newCard.denominations = Denomination.decode(deno, id: newCard.id)
                    }
                    result.append(newCard)
                }
            }
            return result
        } catch let error {
            print("error \(error)")
            return []
        }
    }
}

class Denomination: Object, Codable {
    @objc dynamic var id: String = ""
    @objc dynamic var price: Int = 0
    @objc dynamic var currency: String = ""
    @objc dynamic var stock: String = ""
    @objc dynamic var qty: Int = 0
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func isOutOfStock() -> Bool {
        return stock == "OUT_OF_STOCK"
    }
    
    func hasQuantity() -> Bool {
        return qty > 0
    }
    
    static func decode(_ json: [[String: Any]], id: String) -> List<Denomination> {
        let denominations: List<Denomination> = List<Denomination>()
        json.forEach { eachJson in
            let newDenomination = Denomination()
            
            if let priceInt = eachJson["price"] as? Int {
                newDenomination.price = priceInt
            }
            if let priceString = eachJson["price"] as? String, let priceInt = Int(priceString) {
                newDenomination.price = priceInt
            }
            
            newDenomination.id = "\(id)_\(newDenomination.price)"
            
            if let currency = eachJson["currency"] as? String {
                newDenomination.currency = currency
            }
            
            if let stock = eachJson["stock"] as? String {
                newDenomination.stock = stock
            }
            denominations.append(newDenomination)
        }
        return denominations
    }
}

//
//  User.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 16/5/21.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var email: String = ""
    @objc dynamic var session: String = ""
    @objc dynamic var appleAuthId: String?
    
    override static func primaryKey() -> String? {
        return "email"
    }
}

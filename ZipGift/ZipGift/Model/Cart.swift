//
//  Cart.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 16/5/21.
//

import RealmSwift

class Cart: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var giftCard: GiftCard?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

//
//  DatabaseService.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import Foundation
import RealmSwift

class DatabaseService: NSObject {
    
    private var realm: Realm!
    
    init(_ realm: Realm = try! Realm()) {
        super.init()
        self.realm = realm
    }
    
    func addObject(_ object: Object, update: Realm.UpdatePolicy = .modified) {
        self.addObjects([object], update: update)
    }
    
    func addObjects(_ objects: [Object], update: Realm.UpdatePolicy = .modified) {
        
        try! self.realm.write {
            self.realm.add(objects, update: update)
        }
    }
    
    func removeObject(_ object: Object) {
        try! self.realm.write({
            realm.delete(object)
        })
    }
    
    func removeObjects(_ objects: [Object]) {
        try! self.realm.write({
            realm.delete(objects)
        })
    }
    
    func update(_ block: @escaping (() -> Void)) {
        try! self.realm.write({
            block()
        })
    }
    
    func getObjects<Element: Object>(_ type: Element.Type) -> Results<Element> {
        
        return self.realm.objects(type)
    }
}

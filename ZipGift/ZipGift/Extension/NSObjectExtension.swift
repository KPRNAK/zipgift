//
//  StringExtension.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 14/5/21.
//

import Foundation

extension NSObject {
    public static var className: String {
        return String(describing: self)
    }
}

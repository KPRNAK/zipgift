//
//  StringExtension.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 16/5/21.
//

import Foundation

extension String {

    func toHTMLString() -> NSAttributedString? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }
        return try? NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
}

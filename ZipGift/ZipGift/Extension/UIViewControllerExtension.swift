//
//  UIViewControllerExtension.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit

extension UIViewController {
    
    func showAlertWith(title: String, message: String, actionTitle: String, handler: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: handler))
        self.present(alert, animated: true)
    }
}

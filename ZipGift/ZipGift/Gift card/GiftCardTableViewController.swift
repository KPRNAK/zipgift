//
//  GiftCardViewController.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RxSwift
import SkeletonView

class GiftCardTableViewController: UITableViewController {
    
    private var viewModel = GiftCardListViewModel(with: NetworkService(), databaseService: DatabaseService())
    private var disposeBag = DisposeBag()
    private let refresh = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        tableView.dataSource = self
        tableView.refreshControl = refresh
        tableView.register(GiftCardTableViewCell.self, forCellReuseIdentifier: GiftCardTableViewCell.className)
        tableView.isSkeletonable = true
        setupSearchController()
        
        viewModel.fetchGiftCards()
        setupObserver()
    }
    
    private func setupObserver() {
        viewModel.giftCardDidUpdate
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        viewModel.isLoading
            .bind(to: refresh.rx.isRefreshing)
            .disposed(by: disposeBag)
        
        viewModel.shouldShowLoading
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] shouldShowLoadingAnimation in
                guard let self = self else { return }
                self.tableView.reloadData()
                self.navigationItem.searchController?.searchBar.isUserInteractionEnabled = !shouldShowLoadingAnimation
            }).disposed(by: disposeBag)
        
        refresh.rx
            .controlEvent(.valueChanged)
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.fetchGiftCards()
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.shouldShowLoadingState() {
            return viewModel.numberOfMockLoadingCell
        }
        
        if isSearching() {
            return self.viewModel.searchGiftCardsCount()
        } else {
            return self.viewModel.giftCardsCount()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GiftCardTableViewCell.className, for: indexPath) as? GiftCardTableViewCell else {
            return UITableViewCell()
        }
        
        if viewModel.shouldShowLoadingState() {
            cell.setViewModel(viewModel.mockLoadingViewModel())
            cell.showLoadingAnimation()
            return cell
        } else {
            cell.hideLoadingAnimation()
        }
        
        var currentViewModel: GiftCardViewModel!
        if isSearching() {
            currentViewModel = viewModel.searchViewModel(at: indexPath.row)
        } else {
            currentViewModel = viewModel.viewModel(at: indexPath.row)
        }
        
        cell.setViewModel(currentViewModel)
        cell.onDetailButtonDidTap { [weak self] in
            guard let self = self else { return }
            self.goToDetailVC(withViewModel: currentViewModel)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        goToDetailVC(withViewModel: viewModel.viewModel(at: indexPath.row))
    }
    
    private func goToDetailVC(withViewModel viewModel: GiftCardViewModel) {
        let detailVC = GiftCardDetailTableViewController(style: .grouped)
        detailVC.viewModel = viewModel
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

// MARK: - Setup Search Bar
extension GiftCardTableViewController: UISearchControllerDelegate {
    
    func setupSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search brand"
        self.navigationItem.searchController = searchController
        self.observeSearchInput()
    }
    
    private func observeSearchInput() {
        self.navigationItem.searchController?.searchBar.rx
            .text
            .map { $0 ?? "" }
            .filter { !$0.isEmpty }
            .debounce(.milliseconds(700), scheduler: MainScheduler())
            .asDriver(onErrorJustReturn: "")
            .drive(onNext: { [weak self] text in
                guard let self = self else { return }
                self.viewModel.search(for: text)
            }).disposed(by: disposeBag)
    }
    
    fileprivate func isSearching() -> Bool {
        return self.navigationItem.searchController?.isActive ?? false
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        self.tableView.reloadData()
    }
}

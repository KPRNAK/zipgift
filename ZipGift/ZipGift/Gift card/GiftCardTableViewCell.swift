//
//  GiftCardTableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import SDWebImage
import RxSwift
import SkeletonView

class GiftCardTableViewCell: UITableViewCell {
    
    private var mainStackView: UIStackView!
    private var brandLabel: UILabel!
    private var discountLabel: UILabel!
    private var venderLabel: UILabel!
    private var thumbnailImageView: UIImageView!
    private var detailButton: UIButton!
    private var disposeBag = DisposeBag()
    private var detailButtonDidTap: TapHandler?
    private var viewModel: GiftCardViewModel?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
        self.setupObserver()
    }
    
    private func setupUI() {
        
        isSkeletonable = true
        
        thumbnailImageView = UIImageView()
        thumbnailImageView.contentMode = .scaleAspectFit
        thumbnailImageView.isSkeletonable = true
        
        detailButton = UIButton(type: .infoLight)
        detailButton.isSkeletonable = true
        
        let textStackView = textStackView()
        
        mainStackView = UIStackView(arrangedSubviews: [thumbnailImageView, textStackView, detailButton])
        mainStackView.axis = .horizontal
        mainStackView.alignment = .center
        mainStackView.distribution = .fill
        mainStackView.spacing = 16
        
        contentView.addSubview(mainStackView)
        
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            mainStackView.heightAnchor.constraint(equalToConstant: 100),
            thumbnailImageView.widthAnchor.constraint(equalToConstant: 100),
            thumbnailImageView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100),
            textStackView.heightAnchor.constraint(equalToConstant: 100)
        ])
        
        self.setupObserver()
    }
    
    private func textStackView() -> UIStackView {
        
        brandLabel = label(from: "Brand")
        discountLabel = label(from: "Discount")
        venderLabel = label(from: "Vender")
        
        let stackView = UIStackView(arrangedSubviews: [brandLabel, discountLabel, venderLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    private func label(from text: String) -> UILabel {
        let label = UILabel()
        label.isSkeletonable = true
        label.text = text
        label.font = .systemFont(ofSize: 16)
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }
    
    private func setupObserver() {
        detailButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.detailButtonDidTap?()
            }).disposed(by: disposeBag)
    }
    
    func onDetailButtonDidTap(_ handler: @escaping TapHandler) {
        detailButtonDidTap = handler
    }
    
    func setViewModel(_ viewModel: GiftCardViewModel) {
        
        viewModel.brand
            .bind(to: brandLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.discount
            .bind(to: discountLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.vender
            .bind(to: venderLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.thumbnailUrl
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] url in
                guard let self = self else { return }
                self.thumbnailImageView.sd_setImage(with: url)
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Animations
    
    private func viewsToAnimate() -> [UIView] {
        return [thumbnailImageView, brandLabel, venderLabel, discountLabel, detailButton]
    }
    
    func showLoadingAnimation() {
        DispatchQueue.main.async {
            self.viewsToAnimate().forEach { $0.showAnimatedGradientSkeleton() }
        }
    }
    
    func hideLoadingAnimation() {
        DispatchQueue.main.async {
            self.viewsToAnimate().forEach { $0.hideSkeleton() }
        }
    }
}

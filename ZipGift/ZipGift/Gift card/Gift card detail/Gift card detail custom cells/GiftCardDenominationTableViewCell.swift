//
//  GiftCardDenominationTableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 14/5/21.
//

import UIKit
import RxSwift

class GiftCardDenominationTableViewCell: UITableViewCell {
    
    private var mainStackView: UIStackView!
    private var titleLabel: UILabel!
    private var amountLabel: UILabel!
    private var stepper: UIStepper!
    private var disposeBag = DisposeBag()
    private var stepperValueChanged: TapTypeHandler?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    func set(denomination: Denomination) {
        stepper.value = Double(denomination.qty)
        titleLabel.text = "\(denomination.currency) \(denomination.price)"
        amountLabel.text = "\(denomination.qty) QTY"
        if denomination.isOutOfStock() {
            showAsOutOfStock()
        } else {
            showAsAvailable()
        }
        setupObserver()
    }
    
    func onStepperValueChanged(_ handler: @escaping TapTypeHandler) {
        stepperValueChanged = handler
    }
    
    // MARK: - Private methods
    private func setupUI() {
        
        titleLabel = UILabel()
        titleLabel.font = .systemFont(ofSize: 16)
        titleLabel.textColor = .black
        
        amountLabel = UILabel()
        amountLabel.font = .systemFont(ofSize: 16)
        amountLabel.textColor = .black
        
        stepper = UIStepper()
        stepper.minimumValue = 0
        
        mainStackView = UIStackView(arrangedSubviews: [titleLabel, amountLabel, stepper])
        mainStackView.spacing = 16
        mainStackView.axis = .horizontal
        mainStackView.alignment = .fill
        mainStackView.distribution = .fill
        contentView.addSubview(mainStackView)
        
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupObserver() {
        stepper
            .rx
            .controlEvent(.valueChanged)
            .map { [weak self] _ -> String in
                guard let self = self else { return "" }
                let value = Int(self.stepper.value)
                self.stepperValueChanged?(value)
                return "\(value) QTY"
            }
            .bind(to: amountLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    private func showAsOutOfStock() {
        isUserInteractionEnabled = false
        contentView.alpha = 0.5
        
        if let text = titleLabel.text {
            titleLabel.text = "\(text) OUT OF STOCK"
        }
    }
    
    private func showAsAvailable() {
        isUserInteractionEnabled = true
        contentView.alpha = 1.0
    }
}

//
//  GiftCardImageTableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 14/5/21.
//

import UIKit
import RxSwift
import RxCocoa

class GiftCardImageTableViewCell: UITableViewCell {
    
    private var thumbnailImageView: UIImageView!
    private var disposeBag = DisposeBag()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func setupUI() {
        
        isUserInteractionEnabled = false
        thumbnailImageView = UIImageView()
        thumbnailImageView.contentMode = .scaleAspectFit
        
        backgroundColor = .clear
        contentView.addSubview(thumbnailImageView)
        
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            thumbnailImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            thumbnailImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            thumbnailImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            thumbnailImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            thumbnailImageView.heightAnchor.constraint(equalToConstant: 230)
        ])
    }
    
    func set(viewModel: GiftCardViewModel) {
        viewModel.thumbnailUrl
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] url in
                guard let self = self else { return }
                self.thumbnailImageView.sd_setImage(with: url)
            }).disposed(by: disposeBag)
    }
}

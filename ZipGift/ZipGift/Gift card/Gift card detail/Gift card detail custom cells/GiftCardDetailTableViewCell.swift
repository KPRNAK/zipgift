//
//  GiftCardDetailTableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 14/5/21.
//

import UIKit

class GiftCardDetailTableViewCell: UITableViewCell {
    
    private var mainStackView: UIStackView!
    private var leftLabel: UILabel!
    private var rightLabel: UITextView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        leftLabel.text = ""
        rightLabel.text = ""
    }
    
    private func setupUI() {
        
        leftLabel = UILabel()
        leftLabel.textColor = .systemBlue
        leftLabel.font = .systemFont(ofSize: 16)
        leftLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        
        rightLabel = UITextView()
        rightLabel.textColor = .black
        rightLabel.font = .systemFont(ofSize: 16)
        rightLabel.isScrollEnabled = false
        rightLabel.isEditable = false
        rightLabel.dataDetectorTypes = .all
        
        mainStackView = UIStackView(arrangedSubviews: [leftLabel, rightLabel])
        mainStackView.axis = .horizontal
        mainStackView.alignment = .firstBaseline
        mainStackView.distribution = .fillProportionally
        mainStackView.spacing = 16
        contentView.addSubview(mainStackView)
        
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leftLabel.widthAnchor.constraint(equalToConstant: 70),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    func set(leftText: String, rightText: String) {
        leftLabel.text = leftText
        
        if let htmlText = rightText.toHTMLString() {
            rightLabel.attributedText = htmlText
        } else {
            rightLabel.text = rightText
        }
        rightLabel.font = .systemFont(ofSize: 16)
    }
}

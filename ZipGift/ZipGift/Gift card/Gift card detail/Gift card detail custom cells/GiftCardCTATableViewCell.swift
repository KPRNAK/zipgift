//
//  GiftCardCTATableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 14/5/21.
//

import UIKit
import RxSwift
import RxCocoa

class GiftCardCTATableViewCell: UITableViewCell {
    
    private var mainStackView: UIStackView!
    private var buyNowButton: UIButton!
    private var addToCartButton: UIButton!
    private var disposeBag = DisposeBag()
    private var onButtonDidTap: TapTypeHandler?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupObserver()
    }
    
    private func setupUI() {
        
        buyNowButton = UIButton()
        buyNowButton.setTitle("Buy now", for: .normal)
        buyNowButton.setTitleColor(.blue, for: .normal)
        
        addToCartButton = UIButton()
        addToCartButton.setTitle("Add to cart", for: .normal)
        addToCartButton.setTitleColor(.systemBlue, for: .normal)
        
        mainStackView = UIStackView(arrangedSubviews: [buyNowButton, addToCartButton])
        mainStackView.spacing = 16
        mainStackView.axis = .horizontal
        mainStackView.alignment = .fill
        mainStackView.distribution = .fillEqually
        contentView.addSubview(mainStackView)
        
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
        
        setupObserver()
    }
    
    private func setupObserver() {
        disposeBag = DisposeBag()
        
        buyNowButton.rx
            .tap
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.onButtonDidTap?(GiftCardCTAButtonType.BUY_NOW)
            }).disposed(by: disposeBag)
        
        addToCartButton.rx
            .tap
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.onButtonDidTap?(GiftCardCTAButtonType.ADD_TO_CART)
            }).disposed(by: disposeBag)
    }
    
    func onButtonDidTap(_ handler: @escaping TapTypeHandler) {
        onButtonDidTap = handler
    }
}

enum GiftCardCTAButtonType {
    case BUY_NOW
    case ADD_TO_CART
}

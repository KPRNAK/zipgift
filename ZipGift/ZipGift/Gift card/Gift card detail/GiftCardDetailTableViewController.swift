//
//  GiftCardDetailTableViewController.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit

typealias TapHandler = (() -> Void)
typealias TapTypeHandler = ((Any) -> Void)

class GiftCardDetailTableViewController: UITableViewController {
    
    private enum GiftDetailSection {
        case image
        case detail
        case denomination
        case cta
    }
    
    private var sections: [GiftDetailSection] = [.image, .denomination, .detail, .cta]
    var viewModel: GiftCardViewModel!
    var shouldHideCTA: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "\(viewModel.giftCard.brand) gift card"
        tableView.register(GiftCardImageTableViewCell.self, forCellReuseIdentifier: GiftCardImageTableViewCell.className)
        tableView.register(GiftCardDetailTableViewCell.self, forCellReuseIdentifier: GiftCardDetailTableViewCell.className)
        tableView.register(GiftCardDenominationTableViewCell.self, forCellReuseIdentifier: GiftCardDenominationTableViewCell.className)
        tableView.register(GiftCardCTATableViewCell.self, forCellReuseIdentifier: GiftCardCTATableViewCell.className)
        
        if shouldHideCTA {
            sections.removeLast()
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch sections[section] {
        case .image: return 1
        case .detail: return 3
        case .denomination: return viewModel.denominationCount()
        case .cta: return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .image: return imageCell(tableView, cellForRowAt: indexPath)
        case .detail: return detailCell(tableView, cellForRowAt: indexPath)
        case .denomination: return denominationCell(tableView, cellForRowAt: indexPath)
        case .cta: return ctaCell(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Custom cell
extension GiftCardDetailTableViewController {
    
    private func imageCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GiftCardImageTableViewCell.className, for: indexPath) as? GiftCardImageTableViewCell else {
            return UITableViewCell()
        }
        cell.set(viewModel: viewModel)
        return cell
    }
    
    private func detailCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GiftCardDetailTableViewCell.className, for: indexPath) as? GiftCardDetailTableViewCell else {
            return UITableViewCell()
        }
        switch indexPath.row {
        case 0: cell.set(leftText: "Brand", rightText: viewModel.giftCard.brand)
        case 1: cell.set(leftText: "Discount", rightText: "\(viewModel.giftCard.discount)")
        case 2: cell.set(leftText: "Terms", rightText: viewModel.giftCard.terms)
        default: break
        }
        
        return cell
    }
    
    private func denominationCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GiftCardDenominationTableViewCell.className, for: indexPath) as? GiftCardDenominationTableViewCell else {
            return UITableViewCell()
        }
        cell.set(denomination: viewModel.denomination(at: indexPath.row))
        cell.onStepperValueChanged { [weak self] value in
            guard let self = self, let stepperValue = value as? Int else { return }
            self.viewModel.updateDenomination(at: indexPath.row, qty: stepperValue)
        }
        return cell
    }
    
    private func ctaCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GiftCardCTATableViewCell.className, for: indexPath) as? GiftCardCTATableViewCell else {
            return UITableViewCell()
        }
        cell.onButtonDidTap { [weak self] ctaType in
            guard let self = self else { return }
            self.onCTAButtonDidTap(ctaType)
        }
        return cell
    }
    
    // MARK: - CTA button events handler
    
    private func onCTAButtonDidTap(_ ctaType: Any) {
        guard let type = ctaType as? GiftCardCTAButtonType else {
            return
        }
        
        guard self.viewModel.hasQuantity() else {
            showNoQtyAlert()
            return
        }
        
        switch type {
        case .ADD_TO_CART: showAddToCartConfirmationAlert()
        case .BUY_NOW: showBuyNowConfirmationAlert()
        }
    }
    
    private func showNoQtyAlert() {
        showAlertWith(title: "No quantity", message: "Please add any card quantity", actionTitle: "OK!")
    }
    
    private func showBuyNowConfirmationAlert() {
        showAlertWith(title: "Success", message: "You have successfully bought this gift card.", actionTitle: "Nice 👍🏻") { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.resetGiftCardQuantity()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func showAddToCartConfirmationAlert() {
        viewModel.addToCart()
        showAlertWith(title: "Success", message: "You have successfully added gift card to shopping cart.", actionTitle: "Nice 👍🏻") { [weak self] _ in
            guard let self = self else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//
//  TabBarViewController.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RxSwift
import RxCocoa

class TabBarViewController: UITabBarController {
    
    private var viewModel: CartListViewModel!
    private var disposeBag: DisposeBag!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupViewControllers()
        setupObserver()
    }
    
    // MARK: - Private methods
    
    private func setupViewControllers() {
        viewControllers = [embedNavigationVC(in: GiftCardTableViewController(), item: .GIFT_CARD),
                           embedNavigationVC(in: ShoppingCartViewController(), item: .CART)]
    }
    
    private func embedNavigationVC(in vc: UIViewController, item: TabbarItem) -> UINavigationController {
        
        let navVC = UINavigationController(rootViewController: vc)
        navVC.tabBarItem.title = item.title
        navVC.tabBarItem.image = item.barImage
        navVC.tabBarItem.tag = item.tag
        navVC.navigationBar.prefersLargeTitles = true
        vc.navigationItem.title = item.title
        return navVC
    }
    
    private func setupObserver() {
        
        disposeBag = DisposeBag()
        
        viewModel = CartListViewModel(with: NetworkService(), databaseService: DatabaseService())
        viewModel.totalGiftCardCount
            .map { $0 > 0 ? "\($0)" : nil }
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] totalCartcount in
                guard let self = self else { return }
                self.setCartBadge(totalCartcount)
            }).disposed(by: disposeBag)
    }
    
    private func setCartBadge(_ value: String?) {
        tabbarItemFor(.CART)?.badgeValue = value
    }
    
    private func tabbarItemFor(_ item: TabbarItem) -> UITabBarItem? {
        return tabBar.items?.first(where: { $0.tag == item.tag })
    }
}

// MARK: - Tabbar item

/// Use this to create custom tabbar items
private enum TabbarItem {
    case GIFT_CARD
    case CART
    
    var title: String {
        get {
            switch self {
            case .GIFT_CARD: return "Gift cards"
            case .CART: return "Cart"
            }
        }
    }
    
    var barImage: UIImage? {
        get {
            switch self {
            case .GIFT_CARD: return UIImage(named: "gift-card")
            case .CART: return UIImage(named: "shopping-cart")
            }
        }
    }
    
    var tag: Int {
        get {
            switch self {
            case .GIFT_CARD: return 1000
            case .CART: return 1001
            }
        }
    }
}

//
//  GiftCardViewModel.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import Foundation
import RxSwift
import RxCocoa

class GiftCardViewModel: BaseViewModel {

    private(set) var giftCard: GiftCard
    
    var thumbnailUrl = BehaviorSubject<URL?>(value: nil)
    var brand = BehaviorSubject<String>(value: "")
    var discount = BehaviorSubject<String>(value: "")
    var vender = BehaviorSubject<String>(value: "")
    var shouldDisabled = BehaviorSubject<Bool>(value: false)
    
    init(withModel model: GiftCard, networkService: APIService, databaseService: DatabaseService? = nil) {
        self.giftCard = model
        super.init(with: networkService, databaseService: databaseService)
        updateObjects()
    }
    
    // MARK: - Private methods
    
    /// This method will update all publishable fields.
    private func updateObjects() {
        
        if let url = URL(string: giftCard.image) {
            thumbnailUrl.onNext(url)
        }
        
        brand.onNext(giftCard.brand)
        discount.onNext("\(giftCard.discount)")
        vender.onNext(giftCard.vendor)
    }
    
    // MARK: - Public methods
    
    /// This method all Denomination count that related to current GiftCard object.
    /// - Returns: Denomination objects count
    func denominationCount() -> Int {
        return giftCard.denominations.count
    }
    
    /// This method return a Denomination with given idex.
    /// - Parameter index: index to get.
    /// - Returns: Denomination object from current GiftCard base on index.
    func denomination(at index: Int) -> Denomination {
        return giftCard.denominations[index]
    }
    
    /// This method will update qty of Denomination object in to local cache db.
    /// - Parameters:
    ///   - index: index of GiftCard Denominations object
    ///   - qty: new quantity to update
    func updateDenomination(at index: Int, qty: Int) {
        
        guard let databaseService = self.databaseService else {
            return
        }
        
        databaseService.update { [weak self] in
            guard let self = self else { return }
            self.giftCard.denominations[index].qty = qty
        }
    }
    
    /// This method if any of the Denomination in current GiftCard object has quantity set to more than one.
    /// - Returns: true meaning has quantity set more than one, false meaning has no quantity set more than one in all of Denomination objects.
    func hasQuantity() -> Bool {
        return !giftCard.denominations.filter { $0.hasQuantity() }.isEmpty
    }
    
    /// This method will mark current GiftCard object as addedInCart local cache db.
    func addToCart() {
        
        guard let databaseService = self.databaseService else {
            return
        }
        
        let cloneGiftCard = self.clonedGiftCard()
        let giftCard = Cart()
        giftCard.id = self.giftCard.id
        giftCard.giftCard = cloneGiftCard
        databaseService.addObject(giftCard)
        
        resetGiftCardQuantity()
    }
    
    /// Reset denominations that were set initially by user.
    func resetGiftCardQuantity() {
        guard let databaseService = self.databaseService else {
            return
        }
        databaseService.update {
            self.giftCard.denominations.filter { $0.hasQuantity() }.forEach { $0.qty = 0 }
        }
    }
    
    // MARK: - Private methods
    
    /// Clone entire GiftCard for adding into cart so that they are able to independently modify.
    /// - Returns: a clone GiftCard of current GiftCard
    private func clonedGiftCard() -> GiftCard {
        let cloneGiftCard = GiftCard(value: self.giftCard)
        cloneGiftCard.id = UUID().uuidString
        cloneGiftCard.denominations.removeAll()
        cloneGiftCard.addedInCart = true
        
        self.giftCard.denominations.forEach { eachDeno in
            let cloneDeno = Denomination(value: eachDeno)
            cloneDeno.id = UUID().uuidString
            cloneGiftCard.denominations.append(cloneDeno)
        }
        return cloneGiftCard
    }
}

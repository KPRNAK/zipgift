//
//  SignInViewModel.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 16/5/21.
//

import Foundation
import RxSwift
import LocalAuthentication
import AuthenticationServices

class SignInViewModel: BaseViewModel {

    private var bioContext = LAContext()
    private var correctPassword = "1234"
    var signInSuccess = PublishSubject<Void>()
    var signInError = PublishSubject<SignInError>()
    var bioAuthError = PublishSubject<Error>()
    var appleCredentialValid = PublishSubject<()>()
    var appleCredentialError = PublishSubject<Error?>()
    
    // MARK: - Public methods
    
    /// This will sign user in. Mimic loading for 1sec.
    /// - Parameters:
    ///   - username: username / email
    ///   - password: correct password is "1234" just for easy testing
    func signInWith(username: String, password: String, appleUserId: String? = nil) {
        isLoading.onNext(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self else { return }
            self.isLoading.onNext(false)
            
            guard self.isValid(username: username, password: password) else {
                self.signInError.onNext(.empty)
                return
            }
            
            if password == self.correctPassword {
                self.saveLoginSession(username, appleAuthId: appleUserId)
                self.signInSuccess.onNext(())
            } else {
                self.signInError.onNext(.incorrectPassword)
            }
        }
    }
    
    /// Check if user has logged in.
    /// - Returns: true is user logged in, false is user has not logged in.
    func hasSignIn() -> Bool {
        guard let databaseService = self.databaseService else {
            return false
        }
        
        return !databaseService.getObjects(User.self).isEmpty
    }
    
    /// Get current logged in user object.
    /// - Returns: User object of current logged in user.
    func currentUser() -> User? {
        guard let databaseService = self.databaseService else {
            return nil
        }
        
        return databaseService.getObjects(User.self).first
    }
    
    /// This method will prompt bio auth if device support it. It ignores if not support.
    /// Will auto sign in if successful. bioAuthError gets called if error
    func biometicAuth() {
        var error: NSError?
        if bioContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            bioContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Log in to your account") { success, error in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    let username = self.currentUser()?.email ?? ""
                    if success {
                        self.signInWith(username: username, password: self.correctPassword)
                    }
                    
                    if let error = error {
                        self.bioAuthError.onNext(error)
                    }
                }
            }
        }
        
        if let _ = error, hasSignIn() {
            self.signInSuccess.onNext(())
        }
    }
    
    @available (iOS 13.0, *)
    func isAppleCredentialStillValid() {
        guard let currentUser = currentUser(), let appleAuthId = currentUser.appleAuthId else {
            return
        }
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: appleAuthId) { [weak self] (credentialState, error) in
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    switch credentialState {
                    case .authorized: self.appleCredentialValid.onNext(())
                    case .revoked, .notFound:
                        self.appleCredentialError.onNext(error)
                        self.removeUserCredential()
                    default:
                        break
                    }
                }
            }
    }
    
    func isLoginWithApple() -> Bool {
        guard let currentUser = currentUser() else {
            return false
        }
        return currentUser.appleAuthId != nil
    }
    
    // MARK: - Private methods
    
    /// Check if username & password are not empty. This basically just check if they arent empty.
    /// - Parameters:
    ///   - username: username / email
    ///   - password: password text
    /// - Returns: true if username & password are not empty, false if they are empty.
    private func isValid(username: String, password: String) -> Bool {
        return !username.isEmpty && !password.isEmpty
    }
    
    /// This will create new user object and generate UUID to mimic logged in session.
    /// - Parameter username: username / email that user specified as part of login process.
    private func saveLoginSession(_ username: String, appleAuthId: String? = nil) {
        
        guard let databaseService = self.databaseService else {
            return
        }
        
        if let currentUser = self.currentUser() {
            databaseService.update {
                if let authId = appleAuthId {
                    currentUser.appleAuthId = authId
                }
            }
        } else {
            let user = User()
            user.email = username
            user.session = UUID().uuidString
            if let authId = appleAuthId {
                user.appleAuthId = authId
            }
            databaseService.addObject(user)
        }
    }
    
    /// This method will remove users object from DB
    private func removeUserCredential() {
        
        guard let databaseService = self.databaseService else {
            return
        }
        
        databaseService.removeObjects(databaseService.getObjects(User.self).toArray())
    }
}

/// To use when sign in error
enum SignInError: LocalizedError {
    case empty
    case incorrectPassword
    
    var localizedDescription: String? {
        switch self {
        case .empty: return "Username or password are empty."
        case .incorrectPassword: return "Incorrect username or password"
        }
    }
}

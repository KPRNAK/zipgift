//
//  CartListViewModel.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import Foundation
import RxSwift
import RxRealm

class CartListViewModel: BaseViewModel {
    
    private var giftCards: [Cart] = []
    private var disposeBag = DisposeBag()
    var giftCardDidUpdate = PublishSubject<Void>()
    var hasGiftCard = BehaviorSubject<Bool>(value: false)
    var totalCost = BehaviorSubject<Int>(value: 0)
    var totalGiftCardCount = BehaviorSubject<Int>(value: 0)
    
    override init(with network: APIService, databaseService: DatabaseService? = nil) {
        super.init(with: network, databaseService: databaseService)
        observeGiftCardInCart()
    }
    
    // MARK: - Private methods
    
    private func observeGiftCardInCart() {
        
        guard let databaseService = databaseService else {
            return
        }
        
        Observable.array(from: databaseService.getObjects(Cart.self))
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] result in
                guard let self = self else { return }
                self.giftCards = result
                self.giftCardDidUpdate.onNext(())
                self.hasGiftCard.onNext(!self.giftCards.isEmpty)
                self.totalCost.onNext(self.totalCartCost())
                self.totalGiftCardCount.onNext(self.giftCards.count)
            }).disposed(by: disposeBag)
    }
    
    private func totalCartCost() -> Int {
        
        var total: Int = 0
        self.giftCards.forEach { eachCart in
            eachCart.giftCard?.denominations.filter({ $0.hasQuantity() }).forEach({ eachDeno in
                total += eachDeno.qty * eachDeno.price
            })
        }
        return total
    }
    
    // MARK: - Public methods
    
    /// Get number of gift cards in cart
    func giftCardsCount() -> Int {
        return self.giftCards.count
    }
    
    /// Get GiftCardViewModel from selected index in the cart list
    /// - Parameter index: index to get from
    /// - Returns: GiftCardViewModel with given index model
    func viewModel(at index: Int) -> GiftCardViewModel {
        
        guard let giftCard = self.giftCards[index].giftCard else {
            return GiftCardViewModel(withModel: GiftCard(), networkService: networkService)
        }
        
        return GiftCardViewModel(withModel: giftCard, networkService: networkService, databaseService: databaseService)
    }
    
    /// This will remove gift card from cart local cache db.
    /// - Parameter index: index of GiftCard to remove
    func removeGiftCard(at index: Int) {
        guard let databaseService = databaseService else {
            return
        }
        
        databaseService.removeObject(self.giftCards[index])
    }
    
    /// This will remove all GiftCards in the cart list.
    func checkoutAllGiftCards() {
        guard let databaseService = databaseService else {
            return
        }
        
        databaseService.removeObjects(databaseService.getObjects(Cart.self).toArray())
    }
}

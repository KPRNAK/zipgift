//
//  GiftCardListViewModel.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import Foundation
import RxSwift

class GiftCardListViewModel: BaseViewModel {
    
    private var giftCards: [GiftCard] = []
    private var filterGiftCards: [GiftCard] = []
    private var disposeBag = DisposeBag()
    var giftCardDidUpdate = PublishSubject<Void>()
    var shouldShowLoading = BehaviorSubject<Bool>(value: false)
    var numberOfMockLoadingCell = 7
    
    override init(with network: APIService, databaseService: DatabaseService? = nil) {
        super.init(with: network, databaseService: databaseService)
        setupObserver()
    }
    
    // MARK: - Networking
    
    /// This method will fetch all gift cards from endpoint. Then it will save all those data into DB if success, if failed will nothing get saved.
    func fetchGiftCards() {
        
        isLoading.onNext(true)
        self.networkService.getGiftCarts { [weak self] giftCards in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.isLoading.onNext(false)
                
                guard let databaseService = self.databaseService else {
                    return
                }
                databaseService.addObjects(giftCards)
            }
        }
    }
    
    // MARK: - Public methods
    
    /// This method get number of giftcards cache in local db.
    /// - Returns: number of all giftcards in local db.
    func giftCardsCount() -> Int {
        return giftCards.count
    }
    
    /// This method init viewmodel with given index. Will also pass forward current network service.
    /// - Parameter index: index to generate GiftCardViewModel
    /// - Returns: GiftCardViewModel
    func viewModel(at index: Int) -> GiftCardViewModel {
        return GiftCardViewModel(withModel: giftCards[index], networkService: networkService, databaseService: databaseService)
    }
    
    // MARK: - Private methods
    
    private func setupObserver() {
        
        guard let databaseService = self.databaseService else {
            return
        }
        
        // Will called every time GiftCard gets added or removed.
        let allGiftCards = Observable.array(from: databaseService.getObjects(GiftCard.self).filter("addedInCart == false").sorted(byKeyPath: "position", ascending: true))
            .share(replay: 1)
        
        allGiftCards
            .observe(on: MainScheduler())
            .subscribe(onNext: { result in
                self.giftCards = result
                self.giftCardDidUpdate.onNext(())
            })
            .disposed(by: disposeBag)
        
        allGiftCards
            .map({ $0.isEmpty })
            .observe(on: MainScheduler())
            .bind(to: shouldShowLoading)
            .disposed(by: disposeBag)
    }
    
    // MARK: - Loading methods
    
    /// This method check if should show loading state.
    /// - Returns: true should show, false should hide animation.
    func shouldShowLoadingState() -> Bool {
        return (try? shouldShowLoading.value()) ?? false
    }
    
    /// This method init mock GiftCardViewModel object to use in loading animation state.
    /// brand and vendor value define how long animation will look like.
    /// - Returns: GiftCardViewModel object for animation.
    func mockLoadingViewModel() -> GiftCardViewModel {
        let card = GiftCard()
        card.brand = "          "
        card.vendor = "         "
        return GiftCardViewModel(withModel: card, networkService: networkService, databaseService: databaseService)
    }
    
    // MARK: - Search methods
    
    /// This method init viewmodel with given index when calling search(for key: String). Will also pass forward current network service.
    /// - Parameter index: index to generate GiftCardViewModel
    /// - Returns: GiftCardViewModel
    func searchViewModel(at index: Int) -> GiftCardViewModel {
        if index >= filterGiftCards.count {
            return viewModel(at: index)
        }
        return GiftCardViewModel(withModel: filterGiftCards[index], networkService: networkService, databaseService: databaseService)
    }
    
    /// This method will search for keyword that contain brand in local db.
    /// - Parameter key: keyward to search that contains in giftcard brand.
    func search(for key: String) {
        filterGiftCards = giftCards.filter({ $0.brand.contains(key) })
        giftCardDidUpdate.onNext(())
    }
    
    /// This method get number of current search keyword from local db after calling
    /// - Returns: number of search giftcards in local db that match search keyword.
    func searchGiftCardsCount() -> Int {
        return filterGiftCards.count
    }
}

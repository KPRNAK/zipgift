//
//  BaseViewModel.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import RxCocoa
import RxSwift

class BaseViewModel {
    
    private(set) var networkService: APIService
    var isLoading = PublishSubject<Bool>()
    private(set) var databaseService: DatabaseService?
    
    init(with network: APIService, databaseService: DatabaseService? = nil) {
        self.networkService = network
        self.databaseService = databaseService
    }
    
    func setDatabaseService(_ databaseService: DatabaseService) {
        self.databaseService = databaseService
    }
}

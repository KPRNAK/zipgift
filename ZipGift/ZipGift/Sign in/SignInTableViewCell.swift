//
//  SignInTableViewCell.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RxSwift

class SignInTableViewCell: UITableViewCell {
    
    private var mainStackView: UIStackView!
    private var textField: UITextField!
    private var signInType: SignInType!
    private var textDidChanged: ((String, SignInType) -> Void)?
    private var disposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
        setupObserver()
    }
    
    // MARK: - Public methods
    
    func config(for type: SignInType) {
        signInType = type
        textField.placeholder = type.placeholder
        textField.isSecureTextEntry = type.isSecureText
        textField.textContentType = type.contentType
    }
    
    func onTextDidChanged(_ handler: @escaping ((String, SignInType) -> Void)) {
        textDidChanged = handler
    }
    
    // MARK: - Private methods
    
    private func setupUI() {
        
        textField = UITextField()
        textField.font = .systemFont(ofSize: 16)
        textField.autocapitalizationType = .none
        contentView.addSubview(textField)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            textField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
        
        setupObserver()
    }
    
    private func setupObserver() {
        
        textField.rx
            .controlEvent(.editingChanged)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.textDidChanged?(self.textField.text ?? "", self.signInType)
            }).disposed(by: disposeBag)
    }
}

enum SignInType {
    case username
    case password
    
    var placeholder: String {
        switch self {
        case .username: return "Username / email"
        case .password: return "Password"
        }
    }
    
    var isSecureText: Bool {
        switch self {
        case .password: return true
        default: return false
        }
    }
    
    var contentType: UITextContentType {
        switch self {
        case .username: return .username
        case .password: return .password
        }
    }
}

//
//  SignInViewController.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RxSwift
import RxCocoa
import AuthenticationServices

class SignInViewController: UITableViewController {
    
    private var rows: [SignInType] = [.username, .password]
    private var disposeBag = DisposeBag()
    private var viewModel: SignInViewModel!
    private var signInButton = UIButton()
    private var username: String = ""
    private var password: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = SignInViewModel(with: NetworkService(), databaseService: DatabaseService())
        setupObservers()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SignInTableViewCell.self, forCellReuseIdentifier: SignInTableViewCell.className)
        
        if viewModel.hasSignIn() {
            if viewModel.isLoginWithApple() {
                if #available(iOS 13.0, *) {
                    viewModel.isAppleCredentialStillValid()
                } else {
                    // Fallback on earlier versions
                }
            } else {
                viewModel.biometicAuth()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SignInTableViewCell.className, for: indexPath) as? SignInTableViewCell else {
            return UITableViewCell()
        }
        
        cell.config(for: rows[indexPath.row])
        cell.onTextDidChanged { [weak self] text, type in
            guard let self = self else { return }
            switch type {
            case .username: self.username = text
            case .password: self.password = text
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Private methods
    
    private func footerView() -> UIView {
        
        signInButton.setTitle("Sign In", for: .normal)
        signInButton.setTitleColor(.systemBlue, for: .normal)
        signInButton.setTitleColor(.lightGray, for: .disabled)
        
        let mainStackView = UIStackView(arrangedSubviews: [signInButton])
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        mainStackView.axis = .vertical
        mainStackView.spacing = 32
        mainStackView.distribution = .fillProportionally
        mainStackView.alignment = .center
        
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
            mainStackView.addArrangedSubview(authorizationButton)
        }
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        backgroundView.addSubview(mainStackView)
        
        NSLayoutConstraint.activate([
            mainStackView.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            mainStackView.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor),
            mainStackView.widthAnchor.constraint(equalTo: backgroundView.widthAnchor),
            mainStackView.heightAnchor.constraint(equalTo: backgroundView.heightAnchor)
        ])
        
        return backgroundView
    }
    
    private func goToMainView() {
        UIApplication.shared.delegate?.window??.rootViewController = TabBarViewController()
    }
    
    private func setupObservers() {
        
        viewModel.isLoading
            .map { !$0 }
            .bind(to: signInButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.isLoading
            .map { $0 ? "Loading" : "Sign In" }
            .bind(to: signInButton.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
        viewModel.signInSuccess
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.goToMainView()
            }).disposed(by: disposeBag)
        
        viewModel.signInError
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlertWith(title: "Signin Error", message: error.localizedDescription ?? "", actionTitle: "Try again")
            }).disposed(by: disposeBag)
        
        viewModel.bioAuthError
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlertWith(title: "Signin Error", message: error.localizedDescription , actionTitle: "Try again")
            }).disposed(by: disposeBag)
        
        signInButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.view.endEditing(true)
                self.viewModel.signInWith(username: self.username, password: self.password)
            }).disposed(by: self.disposeBag)
        
        viewModel.appleCredentialValid
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.biometicAuth()
            }).disposed(by: disposeBag)
        
        viewModel.appleCredentialError
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlertWith(title: "Apple log in failed", message: error?.localizedDescription ?? "", actionTitle: "OK")
            }).disposed(by: disposeBag)
    }
}

extension SignInViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    
    @available (iOS 13.0, *)
    @objc private func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let email = appleIDCredential.email ?? ""
            let userIdentifier = appleIDCredential.user
            self.viewModel.signInWith(username: email, password: "1234", appleUserId: userIdentifier)
        default: break
        }
    }
}

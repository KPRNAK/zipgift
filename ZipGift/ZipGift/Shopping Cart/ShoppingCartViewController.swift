//
//  ShoppingCartViewController.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation

class ShoppingCartViewController: UITableViewController {

    private var viewModel: CartListViewModel!
    private var disposeBag = DisposeBag()
    private var checkoutButton = UIButton()
    private var totalCostLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        tableView.register(GiftCardTableViewCell.self, forCellReuseIdentifier: GiftCardTableViewCell.className)
        setupObserver()
    }
    
    private func setupObserver() {
        viewModel = CartListViewModel(with: NetworkService(), databaseService: DatabaseService())
        viewModel.giftCardDidUpdate
            .observe(on: MainScheduler())
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        
        viewModel.totalCost
            .filter { $0 > 0 }
            .map { cost -> String in
                let formater = NumberFormatter()
                formater.numberStyle = .currency
                formater.minimumFractionDigits = 0
                return formater.string(from: NSNumber(value: cost)) ?? "0"
            }
            .map { "Total: \($0)" }
            .bind(to: totalCostLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.totalCost
            .map { $0 <= 0 }
            .bind(to: totalCostLabel.rx.isHidden)
            .disposed(by: disposeBag)
        
        let hasGiftCard = viewModel.hasGiftCard.share(replay: 1)
        
        hasGiftCard
            .bind(to: checkoutButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        hasGiftCard
            .map { $0 ? "Checkout" : "Cart is empty" }
            .bind(to: checkoutButton.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
        checkoutButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.checkoutAllGiftCards()
                self.showCheckoutSuccessConfirmation()
            }).disposed(by: disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.giftCardsCount()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return checkoutFooterView()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GiftCardTableViewCell", for: indexPath) as? GiftCardTableViewCell else {
            return UITableViewCell()
        }
        cell.setViewModel(viewModel.viewModel(at: indexPath.row))
        cell.onDetailButtonDidTap { [weak self] in
            guard let self = self else { return }
            self.goToDetailVC(indexPath.row)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        goToDetailVC(indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.removeGiftCard(at: indexPath.row)
        }
    }
    
    private func goToDetailVC(_ index: Int) {
        let cardDetailVC = GiftCardDetailTableViewController(style: .grouped)
        cardDetailVC.viewModel = viewModel.viewModel(at: index)
        cardDetailVC.shouldHideCTA = true
        self.navigationController?.pushViewController(cardDetailVC, animated: true)
    }
    
    // MARK: Footer Checkout methods
    
    private func checkoutFooterView() -> UIView {
        
        checkoutButton.setTitleColor(.systemBlue, for: .normal)
        checkoutButton.setTitleColor(.gray, for: .disabled)
        
        totalCostLabel.font = .systemFont(ofSize: 16)
        totalCostLabel.textColor = .black
        
        let stackView = UIStackView(arrangedSubviews: [totalCostLabel, checkoutButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        
        let checkoutView = UIView()
        checkoutView.backgroundColor = .white.withAlphaComponent(0.8)
        checkoutView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: checkoutView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: checkoutView.trailingAnchor, constant: -16),
            stackView.topAnchor.constraint(equalTo: checkoutView.topAnchor, constant: 8),
            stackView.bottomAnchor.constraint(equalTo: checkoutView.bottomAnchor, constant: -8),
        ])
        return checkoutView
    }
    
    private func showCheckoutSuccessConfirmation() {
        showAlertWith(title: "Checkout Success", message: "Thanks for buying gift cards from us.", actionTitle: "OK!")
    }
}

//
//  AppDelegate.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 13/5/21.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        configRealmDB()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = signInVC()
        window?.makeKeyAndVisible()
        return true
    }
    
    private func signInVC() -> UINavigationController {
        let vc = SignInViewController(style: .grouped)
        vc.navigationItem.title = "Login"
        
        let navVC = UINavigationController(rootViewController: vc)
        navVC.navigationBar.prefersLargeTitles = true
        
        return navVC
    }
    
    private func configRealmDB() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
        print(Realm.Configuration.defaultConfiguration.fileURL)
    }
}


//
//  APIService.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

typealias GiftCartsList = ([GiftCard]) -> Void

protocol APIService {
    func getGiftCarts(_ handler: @escaping GiftCartsList)
}

//
//  NetworkService.swift
//  ZipGift
//
//  Created by Pichratanak Ky on 15/5/21.
//

import Foundation

struct NetworkService: APIService {
    
    func getGiftCarts(_ handler: @escaping GiftCartsList) {
        
        let url = URL(string: "https://zip.co/giftcards/api/giftcards")!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            guard let data = data else { return }
            handler(GiftCard.decode(data))
        }
        
        task.resume()
    }
}


# Mobile App Dev Guide

This app let user see all avaliable gift cards and items that are already added into cart. User need to login with their credentials. All gift cards are fetched from `https://zip.co/giftcards/api/giftcards`. All data fetch are cache in local Realm db.

## Features
• Show all gift cards
• See gift card detail
• Edit denomination of card and add to Cart or Buy gift card now
• Disabled if denomination is out of stock
• Search gift card by brand name
• Update qty items in cart
• Remove items from cart
• Checkout all items in cart
• Pull down on gift cards tab to refetch
• Support iPad layout, Swift 4.0, iOS 11, Apple Sign in, Biometri auth
• No xib or storyboard

## Building
Please run `pod install` on project directory and open `ZipGift.xcworkspace` once finished.

## Testing
• To login, Username / email can be any string and password is `1234`
• The app must have username and password to get in.
• To add gift card to cart, tab on any card on Gift cards tab. Add at least one qty of denomination and scroll down to bottom, there should be `Add to cart` button. User can't add to cart without qty.
• Buy now should just do nothing, it just pop user back to Gift cards tab.
• To search card, go to Gift card tab, there should be search input on top of the list.
• To remove item in Cart, tap on Cart tab. Swipe right to left to delete an item.
• To edit number of cards user want to purchase, tap on Cart tab. Tap on any card to go to detail and edit.
• To checkout cart, go to Cart tab, there should be a Checkout button. Tap on it, it should clear all items in the cart.
• Items in cart should persist on reopen.
• If testing Biometric auth on simulator iPhone 12, make sure to enroll FaceID by going to `Features-> FaceID -> Enrolled`. Go to `Features-> FaceID -> Matching face` for success
• Biometric wont be enabled in the app if device wont support it.
• It ideal to test Sign in with Apple on physical device. Please use device with iOS 13 and above to see the Login with Apple button in login screen.


## Additional Information
• Skeleton animation on loading all Gift cards the first time if network in slow.
• Red badge on Cart tab shows number of gift cards in the list.
• If next time user launch the app after first log in, the app should prompt Biometric authentication.
• If Biometric authentication is allow by user, user should be able to login with FaceID or TouchID if device has them enrolled.
• If Biometric authentication is NOT allow by user, it should bring user in to Gift cards without reenter username & password the next time user reopen the app.
• If user Sign in with apple, the first time it should log user in if success. The next time open the app, it should promt with Biometric auth.
